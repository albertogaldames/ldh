import processing.serial.*;
/*Maxim maxim;
AudioPlayer Sou;
AudioPlayer SouOver;
AudioPlayer SouWin;
AudioPlayer SouFire;
AudioPlayer SouBob;
AudioPlayer SouAc;
AudioPlayer SouIn;
AudioPlayer SouIn2;*/

PImage Fon;
PImage Jt;
PImage Log;
PImage Ps;

PImage Imgl1;
PImage Imgl2;
PImage Imgd1;
PImage Imgd2;
PImage SaltI;
PImage SalI;
PImage LanI;
PImage LanD;

PImage Bal;
PImage Bal1;
PImage Bal2;
PImage Piso;
PImage Mpa;
PImage Flo;
PImage Over;
PImage Des;
PImage Win;
PImage Win2;
PImage Toa;
PImage Nuv;
PImage Hmar;
PImage Cbos;
PImage Lui;
PImage Lui1;
PImage Lui2;
PImage Text;
PImage Text2;
PImage Text3;
PImage Text4;
PImage TexC;
PImage TexV;
PImage TexA;
PImage Tec;

PImage Dan;
PImage FDan;
PImage FDan1;
PImage FDan2;

PImage Exp;
PImage Fal;
PImage Fal1;
PImage Bow;
PImage Bow2;
PImage Bob;
PImage Fue;
PImage Boc;
PImage Boc1;

int Balx = -10; 
int Baly = -10;

int x1 = 270;
int y2 = 0;
int xs = 305;
int y = 460;
int X = 0;
int Zx = 0;
int Za = 0;
int XX = 0;
float N = 0;
float M = 0;

int Start = 1;
int Pos = 5;
int Salt = 0;
int Punt = 10;
int Fallos = 3;
int F = 0;
int F1 = 0;
int K = 0;
int G = 0;
int S = 0;
int S1 = 0;
int J = 0;
int J1 = 10;

float AtaCx = 0;
float AtaCy = 0;
float Fuex = 500;
float Fuey = -10;

boolean Acer1 = false;
boolean Acer2 = false;
boolean Acer3 = false;

Serial puerto;

void setup() {
  size(700, 600);

  /*maxim = new Maxim(this);
  Sou = maxim.loadFile("Super Mario.wav");
  Sou.speed(1);
  SouOver = maxim.loadFile("Game over.wav");
  SouOver.speed(1);
  SouWin = maxim.loadFile("Win.wav");
  SouWin.speed(1);
  SouFire = maxim.loadFile("Fue.wav");
  SouFire.speed(0.3);
  SouBob = maxim.loadFile("Bob.wav");
  SouBob.speed(0.3);
  SouIn = maxim.loadFile("In.wav");
  SouBob.speed(0.3);
  SouIn2 = maxim.loadFile("In2.wav");
  SouBob.speed(0.3);
  SouAc = maxim.loadFile("Ac.wav");
  SouAc.speed(1);*/

  Fon = loadImage("Fon.png");
  Jt = loadImage("Jt.png");
  Log = loadImage("Log.png");
  Ps = loadImage("Ps.png");

  Bal = loadImage("Bal.png");
  Bal1 = loadImage("Bal1.png");
  Bal2 = loadImage("Bal2.png");
  Piso = loadImage("Piso.png");
  Mpa = loadImage("Mpa.png");
  Flo = loadImage("Flo.png");
  Over = loadImage("Game over.png");
  Des = loadImage("Des.png");
  Win = loadImage("Win.png");
  Win2 = loadImage("Win2.png");
  Toa = loadImage("Toa.png");
  Nuv = loadImage("Nuv.png");
  Lui = loadImage("Lui.png");
  Lui1 = loadImage("Lui1.png");
  Lui2 = loadImage("Lui2.png");
  Text = loadImage("Text.PNG");
  Text2 = loadImage("Text2.PNG");
  Text3 = loadImage("Text3.PNG");
  Text4 = loadImage("Text4.PNG");
  TexC = loadImage("TexC.png");
  TexV = loadImage("TexV.png");
  TexA = loadImage("TexA.png");  
  Tec = loadImage("Tec1.png");

  Imgl1 = loadImage("Img1.png");
  Imgl2 = loadImage("Img2.png");
  Imgd1 = loadImage("Img11.png");
  Imgd2 = loadImage("Img22.png");
  SaltI = loadImage("Salt.png");
  SalI = loadImage("Salt1.png");
  LanD = loadImage("LanD.png");
  LanI = loadImage("LanI.png");

  Exp = loadImage("Exp2.png");
  Fal = loadImage("Fal.png");
  Fal1 = loadImage("Fal1.png");
  Bow = loadImage("Bow.png");
  Bow2 = loadImage("Bow2.png");
  Bob = loadImage("Bob.png");
  Fue = loadImage("Fue.png");
  Hmar = loadImage("Hmar.png");
  Cbos = loadImage("Cbos.png");
  Boc = loadImage("Boc.png");
  Boc1 = loadImage("Boc1.png");

  Dan = loadImage("Dan.png");
  FDan = loadImage("FDan.PNG");
  FDan1 = loadImage("FDan1.PNG");
  FDan2 = loadImage("FDan2.PNG");

  AtaCx = random(520);

  background(255);

  String COM = Serial.list()[0];
  puerto = new Serial(this, COM, 9600);
}

void draw() {
  background(255);

  if (Start==1) {
    if (S==0) {
      //SouAc.play();
      //SouIn.stop();
      image(Fon, 0, 0, 700, 600);
      image(Jt, 450, 480);
      image(Log, 50, 290);
      image(Ps, 30, 420, 380, 180);
    }
    S1 = S1 + 1;
    if (S1>100) {
      S=1;
      //SouIn2.play();
      //SouAc.stop();
      if (K==0) {
      }
      J=J+1;
      println("Valor: "+J);
      if (J>=0&&J<=400) {
        image(Piso, 0, 0);
        image(Mpa, 550, 50);
        image(Bow, AtaCx, 5);
        image(Boc, AtaCx+72, 140);
        image(Lui, 50, 400);
        image(Text, 190, 250);
        if (J%40==0) {
          J1=J1-1;
        }
        if (J==400) {
          J1=10;
        }
        textSize(25);
        fill(0, 102, 153);
        text("Continua en.. "+J1, 450, 580);
      }
      else if (J>400&&J<=800) {
        image(Piso, 0, 0);
        image(Mpa, 550, 50);
        image(Bow, AtaCx, 5);
        image(Boc, AtaCx+72, 140);
        image(Lui, 50, 400);
        image(FDan2, AtaCx+50, 200);
        image(Text2, 190, 250);
        if (J%40==0) {
          J1=J1-1;
        }
        if (J==800) {
          J1=10;
        }
        textSize(25);
        fill(0, 102, 153);
        text("Continua en.. "+J1, 450, 580);
      }
      else if (J>800&&J<=1200) {
        image(Piso, 0, 0);
        image(Lui1, 110, 0);
        image(Text3, 280, 100);
        textSize(25);
        fill(0, 102, 153);
        text(" X "+Punt, 60, 45, 10);
        fill(0, 102, 153);
        text(" X "+Fallos, 60, 580);
        image(Hmar, 10, 540);
        image(Cbos, 10, 10);
        //image(Log, 450, 525, 220, 100);
        image(TexV, 500, 15);
        if (J%40==0) {
          J1=J1-1;
        }
        if (J==1200) {
          J1=10;
        }
        textSize(25);
        fill(0, 102, 153);
        text("Continua en.. "+J1, 450, 580);
      }
      else if (J>1200&&J<=1600) {
        image(Piso, 0, 0);
        image(Lui2, 110, 510);
        image(Text4, 200, 400);
        textSize(25);
        fill(0, 102, 153);
        text(" X "+Punt, 60, 45, 10);
        fill(0, 102, 153);
        text(" X "+Fallos, 60, 580);
        image(Hmar, 10, 540);
        image(Cbos, 10, 10);
        //image(Log, 450, 525, 220, 100);
        image(TexV, 500, 15);
        if (J%40==0) {
          J1=J1-1;
        }
        if (J==1600) {
          J1=10;
        }
        textSize(25);
        fill(0, 102, 153);
        text("Continua en.. "+J1, 450, 580);
      }
      else if (J>1600&&J<=2000) {
        image(Piso, 0, 0);
        image(Tec, 0, 0);
        //image(Log, 450, 525, 220, 100);
        image(TexC, 15, 15);
        if (J%40==0) {
          J1=J1-1;
        }
        if (J==2000) {
          J1=10;
        }
        textSize(25);
        fill(0, 102, 153);
        text("Continua en.. "+J1, 450, 580);
      }
      else if (J>2000&&J<=2400) {
        image(Piso, 0, 0);
        image(TexA, 108, 50);
        if (J%40==0) {
          J1=J1-1;
        }
        if (J==2400) {
          J1=10;
        }
        textSize(50);
        fill(0, 102, 153);
        text("En.. "+J1, 250, 300);
      }
      else {
        if (F>=1) {
          puerto.stop();
          //Sou.stop();
          //SouFire.stop();
          //SouBob.stop();
          //SouIn2.stop();
        }
        else {
          //Sou.play();
          //SouIn2.stop();
        }

        image(Piso, 0, 0);
        image(Mpa, 550, 120);
        image(Flo, 100, 480);
        image(Toa, 590, 5);
        image(Nuv, 575, 60);

        fill(255);

        //>> MOVIMIENTO
        //------------------------------------------------------------    
        //------------------------------------------------------------

        if (Salt==1) {
          XX = x1 + Zx;
          if (XX<0) {
            x1 = 590;
            Zx = 0;
          }
          else if (XX>590) {
            x1=0;
            Zx = 0;
          }
          if (y2==0) {
            y = y - 5;
            image(SalI, XX, y);
            if (y==320) {
              y2=1;
            }
          }
          else if (y2==1) {
            y = y + 5;
            image(SalI, XX, y);
            if (y==460) {
              y2 = 0;
              Salt = 0;
            }
          }
        }
        else {
          if (!Acer2) {
            XX = x1 + Zx;
            if (XX<0) {
              x1 = 590;
              Zx = 0;
            }
            else if (XX>590) {
              x1=0;
              Zx = 0;
            }

            if (Zx<=Za) {
              if ((Zx%2)==0) {
                if (G==0) {
                  image(Imgl1, XX, y);
                }
                else if (G==1) {
                  image(LanI, XX, y);
                }
                else if (G==2) {
                  image(LanD, XX, y);
                }
                else if (G==3) {
                  image(Fal1, XX-37, y+35);
                }
              }
              else {
                if (G==0) {
                  image(Imgl2, XX, y);
                }
                else if (G==1) {
                  image(LanI, XX, y);
                }
                else if (G==2) {
                  image(LanD, XX, y);
                }
                else if (G==3) {
                  image(Fal1, XX-37, y+35);
                }
              }
            }
            else if (Zx>Za) {
              if ((Zx%2)==0) {
                if (G==0) {
                  image(Imgd1, XX, y);
                }
                else if (G==1) {
                  image(LanI, XX, y);
                }
                else if (G==2) {
                  image(LanD, XX, y);
                }
                else if (G==3) {
                  image(Fal, XX, y+35);
                }
              }
              else {
                if (G==0) {
                  image(Imgd2, XX, y);
                }
                else if (G==1) {
                  image(LanI, XX, y);
                }
                else if (G==2) {
                  image(LanD, XX, y);
                }
                else if (G==3) {
                  image(Fal, XX, y+35);
                }
              }
            }
          }
        }

        //-----------------------------------------------------------
        //-----------------------------------------------------------

        //>> DEFENSA
        //-----------------------------------------------------------
        //-----------------------------------------------------------

        if (Pos==0) {
          image(Bal, Balx, Baly);
          Baly = Baly-10;
        }
        else if (Pos==1) {
          image(Bal, Balx, Baly-60);
          Baly = Baly-10;
        }
        else if (Pos==2) {
          image(Bal1, Balx-50, Baly);
          Balx = Balx - 4;
          Baly = Baly - 10;
        }
        else if (Pos==3) {
          image(Bal2, Balx+70, Baly);
          Balx = Balx + 4;
          Baly = Baly - 10;
        }


        //------------------------------------------------------------
        //------------------------------------------------------------

        //>> ATAQUE
        //-----------------------------------------------------------
        //-----------------------------------------------------------

        image(Bob, AtaCx-50, AtaCy+100);
        image(Bow, AtaCx, 5);
        if (Punt<=3) {
          image(Boc1, AtaCx+72, 140);
        }
        else {
          image(Boc, AtaCx+72, 140);
        }
        if (Punt<=6) {
          image(SaltI, AtaCx+85, 105, 30, 30);
        }
        if (Punt<=4) {
          image(SaltI, AtaCx+42, 125, 30, 30);
        }
        if (Punt<=2) {
          image(SaltI, AtaCx+42, 125, 30, 30);
        }

        if (AtaCy>590) {
          AtaCy=0;
          AtaCx=random(520);
          Acer1 = false;
          Acer2 = false;
          Acer3 = false;
        }
        else {
          AtaCy=AtaCy+5;
          if (AtaCy<=150) {
            if (F<=1) {
              //SouBob.play();
            }
            else {
              //SouBob.stop();
            }
          }
          else {
            //SouBob.stop();
          }
        }

        if (Punt<=5) {
          if (Fuex>=-40) {
            Fuey=485;
            image(Bow2, 510, 350);
            image(Fue, Fuex, Fuey);
            Fuex=Fuex-2;
            if (Fuex>=-30) {
              if (F<=1) {
                //SouFire.play();
              }
              else {
                //SouFire.stop();
              }
            }
            else {
              //SouFire.stop();
            }
          }
          else {
            Fuex=500;
          }
        }
        else if (Punt==6) {
          image(Dan, 500, 260);
          image(FDan, 500, 450);
          N = N + 0.5;
          M = N%2;
          if (M==0) {
            image(FDan1, 580, 400);
            image(FDan2, 580, 520);
          }
          else {
            image(FDan1, 570, 400);
            image(FDan2, 570, 520);
          }
        }

        //-----------------------------------------------------------
        //-----------------------------------------------------------

        //>> COLISION
        //-----------------------------------------------------------
        //-----------------------------------------------------------

        if ((Balx>=AtaCx-100)&&(Balx<=AtaCx)) {
          if ((Baly>=AtaCy)&&(Baly<=AtaCy+50)) {
            Acer1=true;
          }
        }
        if ((Balx+25>=AtaCx+98)&&(Balx+25<=AtaCx+170)) {
          if ((Baly>=150)&&(Baly<=197)) {
            Acer3=true;
          }
        }
        if ((AtaCx>=XX)&&(AtaCx<=XX+100)) {
          if ((AtaCy>=y-50)&&(AtaCy<=y)) {
            Acer2=true;
          }
        }
        if ((Fuex>=XX-50)&&(Fuex<=XX+50)) {
          if ((Fuey>=y-50)&&(Fuey<=y+50)) {
            Acer2=true;
          }
        }

        //-----------------------------------------------------------
        //-----------------------------------------------------------

        //>> ACIERTOS
        //-----------------------------------------------------------
        //-----------------------------------------------------------

        if (Acer1) {
          image(Exp, AtaCx-100, AtaCy);

          AtaCx=1000;
          AtaCy=1000;
        }

        if (Acer3) {
          image(Exp, AtaCx+100, 100);

          AtaCx=1000;
          AtaCy=1000;
          Balx=-1000;
          Baly=-1000;
          Punt=Punt-1;
        }

        //-----------------------------------------------------------
        //-----------------------------------------------------------

        //>> FALLOS
        //-----------------------------------------------------------
        //-----------------------------------------------------------
        if (Acer2) {
          G=3;
          AtaCx=1000;
          AtaCy=1000;
          Fallos=Fallos-1;
        }

        //-----------------------------------------------------------
        //-----------------------------------------------------------
        textSize(25);
        fill(0, 102, 153);
        text(" X "+Punt, 60, 45, 10);
        fill(0, 102, 153);
        text(" X "+Fallos, 60, 580);
        image(Hmar, 10, 540);
        image(Cbos, 10, 10);

        if (Punt<=0) {
          if (F<=400) {
            //SouWin.play();
          }
          else {
            //SouWin.stop();
          } 
          image(Piso, 0, 0);
          image(Win, 150, 300);
          image(Des, 400, 10);
          image(Win2, 460, 60);
          Fallos=1000;
          F=F+1;
          Salt=1;
          K = 1;
        }

        if (Fallos<=0) {
          if (F<=400) {
            //SouOver.play();
          }
          else {
            //SouOver.stop();
          } 
          image(Over, -100, -150, 920, 900);
          image(Fal, 305, 428, 100, 60);
          Punt=1000;
          F=F+1;
          Salt=1;
          K = 1;
        }


        //-----------------------------------------------------------
        //-----------------------------------------------------------
      }
    }
  }
  else {
    //SouIn.play();
    image(Fon, 0, 0, 700, 600);
    image(Jt, 450, 480);
    image(Log, 50, 290);
    image(Ps, 60, 430);
  }
  serialRead();
}


void serialRead() {
  int valor  = puerto.read();    
  int accion = puerto.read();    
  Jostinck((char)valor, accion);
}


void Jostinck(char valor, int accion) {
  switch(valor) {
  case 'x':
    X = accion;
    if ((X>=0) && (X<5)) {
      X=X-5;
    }
    else if ((X>5) && (X<=10)) {
      X=X-5;
    }
    Za = Zx;
    Zx = Zx + X;
    G=0;
    break;

  case 'y':
    break;

  case 'b':
    int But = accion;
    if (But==1) {
      G=2;
      Pos=3;
      Balx = XX;
      Baly = y;
    }
    else if (But==2) {
      G=1;
      Pos=1;
      Balx = XX;
      Baly = y;
    }
    else if (But==3) {
      G=1;
      Pos=2;
      Balx = XX;
      Baly = y;
      J=J+2000;
    }
    else if (But==0) {
      Salt=1;
      J=J+2000;
    }
    else if (But==4) {
      Start=1;
    }
    break;
  }
}
