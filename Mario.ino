#define AnaY A0
#define AnaX A1

int valorX;
int valorY;
int x;
int y;
const int Top = 10;
const int Rep = Top/2;

const int Bots = 4;
//const int Sta = 2;
const int Bol = 4;
const int Tria  = 5;
const int Equ = 3;
const int Cuad = 2;
int B[Bots] = { 
  Equ, Bol, Tria, Cuad/*, Sta*/};

void setup(){
  for(int i = 0; i < Bots; i++){
    pinMode(B[i], INPUT);
    digitalWrite(B[i], HIGH);
  }
  pinMode(AnaY, INPUT);
  pinMode(AnaX, INPUT);

  Serial.begin(9600);
}

void loop(){
  valorX = analogRead(AnaX);
  x = map(valorX,0,1024,0,Top+1);

  valorY = analogRead(AnaY);
  y = map(valorY,0,1024,0,Top);

  if(x > Rep || x < Rep){
    Serial.write('x');
    Serial.write((Top+1)-x);
  }

  if(y > Rep || y < Rep){
    Serial.write('y');
    Serial.write(y);
  }  

  for(int i = 0; i < Bots; i++){
    //si hay evento de un boton del Joystick
    if(digitalRead(B[i]) == 1){
      //escribe el identificador del boton
      Serial.write('b');
      //escribe el boton seleccionado
      //Boton derecho   : 0
      //Boton arriba    : 1
      //Boton abajo     : 2
      //Boton izquierdo : 3
      Serial.write(i);
      delay(10);
    }
  }    
  delay(20);
}
